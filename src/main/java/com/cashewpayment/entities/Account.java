
package com.cashewpayment.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This class is the Account entity <br>
 * All below params are mandatory
 * <ol>
 * <li>id - String - Also primary key</li>
 * <li>name - String</li>
 * <li>balance - Float</li>
 * </ol>
 */
@Entity
public class Account {

	@Id
	private String id;
	private String name;
	private Float balance;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the balance
	 */
	public Float getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(Float balance) {
		this.balance = balance;
	}

}
