package com.cashewpayment.service;

import java.util.List;

import com.cashewpayment.entities.Account;

public interface AccountService {

	/**
	 * Save all the accounts in the database 
	 * @author Tauqeer
	 * @since 1.0.0
	 * @return {@link Iterable} of {@link Account} 
	 */
	public Iterable<Account> save(List<Account> accounts);

	/**
	 * Returns all the accounts saved in the database 
	 * @author Tauqeer
	 * @since 1.0.0
	 * @return List of {@link Account} 
	 */
	public List<Account> getAll();
}