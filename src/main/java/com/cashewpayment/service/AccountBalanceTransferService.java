package com.cashewpayment.service;

import com.cashewpayment.dto.TransferBalanceRequestDto;
import com.cashewpayment.dto.TransferBalanceResponseDto;
import com.cashewpayment.entities.Account;
import com.cashewpayment.exception.CustomExceptionSchema;

public interface AccountBalanceTransferService {

	/**
	 * This function transfer the balance from one account to another account.
	 * There are 4 valid error codes that will return in {@link CustomExceptionSchema} <br/>
	 * <ol>
	 * <li>balance-transfer-001 <i>In case source id is invalid</i></li>
	 * <li>balance-transfer-002 <i>In case beneficiary id is invalid</i></li>
	 * <li>balance-transfer-003 <i>In case both account ids' are same</i></li>
	 * <li>balance-transfer-004 <i>In case source account do not have sufficient balance</i></li>
	 * </ol>
	 * @param request {@link TransferBalanceRequestDto} 
	 * @author Tauqeer
	 * @since 1.0.0
	 * @return {@link TransferBalanceResponseDto} 
	 */
	public TransferBalanceResponseDto transfer(TransferBalanceRequestDto request) throws Exception;
}