package com.cashewpayment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cashewpayment.entities.Account;
import com.cashewpayment.repository.AccountRepository;
import com.cashewpayment.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	public Iterable<Account> save(List<Account> accounts) {
		return accountRepository.saveAll(accounts);
	}

	public List<Account> getAll() {
		return accountRepository.findAll();
	}

	public AccountRepository getAccountRepository() {
		return accountRepository;
	}

	public void setAccountRepository(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

}