package com.cashewpayment.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cashewpayment.dto.TransferBalanceRequestDto;
import com.cashewpayment.dto.TransferBalanceResponseDto;
import com.cashewpayment.entities.Account;
import com.cashewpayment.exception.CustomException;
import com.cashewpayment.repository.AccountRepository;
import com.cashewpayment.service.AccountBalanceTransferService;
import com.cashewpayment.utils.CommonUtils;

@Service
public class AccountBalanceTransferServiceImpl implements AccountBalanceTransferService  {

	@Autowired
	private AccountRepository accountRepository;

	@Transactional
	public TransferBalanceResponseDto transfer(TransferBalanceRequestDto request) throws Exception {

		try {
			TransferBalanceResponseDto response = null;
			Optional<Account> sourceAccount = getAccountRepository().findById(request.getSourceAccountId());
			Optional<Account> beneficiaryAccount = getAccountRepository().findById(request.getBeneficiaryAccountId());

			if (!sourceAccount.isPresent()) {
				throw new CustomException("balance-transfer-001", "Source Account Not Found");
			} else if (!beneficiaryAccount.isPresent()) {
				throw new CustomException("balance-transfer-002", "Beneficiary Account Not Found");
			} else if (sourceAccount.get().getId().equalsIgnoreCase(beneficiaryAccount.get().getId())) {
				throw new CustomException("balance-transfer-003", "Beneficiary and source account can not be same");
			} else if (sourceAccount.get().getBalance() < request.getAmount()) {
				throw new CustomException("balance-transfer-004",
						"Requested amount can not be less than source account balance");
			} else {
				Account sourceAccountObj = sourceAccount.get();
				Account beneficiaryAccountObj = beneficiaryAccount.get();
				sourceAccountObj
						.setBalance(CommonUtils.roundFloat(sourceAccountObj.getBalance() - request.getAmount(), 2));
				beneficiaryAccountObj.setBalance(
						CommonUtils.roundFloat(beneficiaryAccountObj.getBalance() + request.getAmount(), 2));

				accountRepository.save(sourceAccountObj);
				accountRepository.save(beneficiaryAccountObj);
				response = new TransferBalanceResponseDto(request.getSourceAccountId(),
						request.getBeneficiaryAccountId(), sourceAccountObj.getBalance(),
						beneficiaryAccountObj.getBalance());
			}

			return response;
		} catch (Exception ex) {
			throw ex;

		} finally {

		}

	}

	public AccountRepository getAccountRepository() {
		return accountRepository;
	}

	public void setAccountRepository(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

}