package com.cashewpayment;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.cashewpayment.entities.Account;
import com.cashewpayment.service.AccountService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is the main application class which loads data from the accounts.json 
 */
@SpringBootApplication
public class CashewPaymentAssignmentApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(CashewPaymentAssignmentApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(AccountService accountService) {
		return args -> {
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Account>> typeReference = new TypeReference<List<Account>>() {
			};
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("accounts.json");
			try {
				List<Account> accounts = mapper.readValue(inputStream, typeReference);
				accountService.save(accounts);
				System.out.println("System is ready to make transfers!");
			} catch (IOException e) {
				System.out.println("Unable to save accounts from json files: " + e.getMessage());
			}
		};
	}

}
