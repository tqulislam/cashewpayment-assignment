package com.cashewpayment.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CommonUtils {

	public static float roundFloat(float f, int places) {

		BigDecimal bigDecimal = new BigDecimal(Float.toString(f));
		bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
		return bigDecimal.floatValue();
	}

}