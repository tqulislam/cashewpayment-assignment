package com.cashewpayment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This Advice is created to intercept the exception from controller API where
 * {@link CustomException} is thrown so that response of the API should have
 * only those fields defined in CustomException class.
 */

@ControllerAdvice
public class CustomExceptionInterceptor extends ResponseEntityExceptionHandler {

	@ExceptionHandler(CustomException.class)
	public final ResponseEntity<Object> handleAllExceptions(CustomException ex) {
		CustomExceptionSchema exceptionResponse = new CustomExceptionSchema(ex.getErrorCode(), ex.getErrorMessage());
		return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}