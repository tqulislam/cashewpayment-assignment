package com.cashewpayment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cashewpayment.dto.TransferBalanceRequestDto;
import com.cashewpayment.dto.TransferBalanceResponseDto;
import com.cashewpayment.entities.Account;
import com.cashewpayment.service.AccountBalanceTransferService;
import com.cashewpayment.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private AccountBalanceTransferService accountTransferService;

	
	/**
	 * This api returns all the accounts available in database<br>
	 * @Json_Request None
	 * @JSON_RESPONSE: List of {@link Account}
	 * @HTTP_VERB GET
	 * @ENDPONT {hostURL}/{contextApi}/account
	 */
	@GetMapping("")
	public ResponseEntity<List<Account>> getAll() {
		return ResponseEntity.ok(accountService.getAll());
	}

	/**
	 * This api transfer the data from one account to another account the accounts available in database<br>
	 * @Json_Request { <br>
    	"beneficiaryAccountId":"someId",<br>
    	"sourceAccountId":"someId",<br>
    	"amount":10 {Amount in float} <br>
		}
	 * @JSON_RESPONSE: {@link TransferBalanceResponseDto}
	 * @HTTP_VERB POST
	 * @ENDPONT {hostURL}/{contextApi}/account/balance/transfer
	 */
	@PostMapping("/balance/transfer")
	public ResponseEntity<TransferBalanceResponseDto> signin(@RequestBody TransferBalanceRequestDto request)
			throws Exception {
		TransferBalanceResponseDto response = accountTransferService.transfer(request);
		return ResponseEntity.ok(response);

	}

}