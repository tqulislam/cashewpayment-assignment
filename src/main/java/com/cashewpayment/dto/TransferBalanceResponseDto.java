package com.cashewpayment.dto;

/**
 * This class is the response dto to receive balance request. <br>
 * All below params will be returned if transfer is successful
 * <ol>
 * <li>sourceAccountId - String</li>
 * <li>beneficiaryAccountId - String</li>
 * <li>sourceAccountAmount - Float</li>
 * <li>beneficiaryAccountAmount - Float</li>
 * </ol>
 */
public class TransferBalanceResponseDto {

	private String sourceAccountId;
	private String beneficiaryAccountId;
	private Float sourceAccountAmount;
	private Float beneficiaryAccountAmount;

	

	/**
	 * 
	 */
	public TransferBalanceResponseDto() {
		super();
	}

	/**
	 * @param sourceAccountId
	 * @param beneficiaryAccountId
	 * @param sourceAccountAmount
	 * @param beneficiaryAccountAmount
	 */
	public TransferBalanceResponseDto(String sourceAccountId, String beneficiaryAccountId, Float sourceAccountAmount,
			Float beneficiaryAccountAmount) {
		super();
		this.sourceAccountId = sourceAccountId;
		this.beneficiaryAccountId = beneficiaryAccountId;
		this.sourceAccountAmount = sourceAccountAmount;
		this.beneficiaryAccountAmount = beneficiaryAccountAmount;
	}

	/**
	 * @return the sourceAccountId
	 */
	public String getSourceAccountId() {
		return sourceAccountId;
	}

	/**
	 * @param sourceAccountId the sourceAccountId to set
	 */
	public void setSourceAccountId(String sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}

	/**
	 * @return the beneficiaryAccountId
	 */
	public String getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}

	/**
	 * @param beneficiaryAccountId the beneficiaryAccountId to set
	 */
	public void setBeneficiaryAccountId(String beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}

	/**
	 * @return the sourceAccountAmount
	 */
	public Float getSourceAccountAmount() {
		return sourceAccountAmount;
	}

	/**
	 * @param sourceAccountAmount the sourceAccountAmount to set
	 */
	public void setSourceAccountAmount(Float sourceAccountAmount) {
		this.sourceAccountAmount = sourceAccountAmount;
	}

	/**
	 * @return the beneficiaryAccountAmount
	 */
	public Float getBeneficiaryAccountAmount() {
		return beneficiaryAccountAmount;
	}

	/**
	 * @param beneficiaryAccountAmount the beneficiaryAccountAmount to set
	 */
	public void setBeneficiaryAccountAmount(Float beneficiaryAccountAmount) {
		this.beneficiaryAccountAmount = beneficiaryAccountAmount;
	}

}
