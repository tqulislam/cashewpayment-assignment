package com.cashewpayment.dto;

import javax.validation.constraints.NotBlank;

/**
 * This class is the request dto to receive balance request. <br>
 * All below params are mandatory
 * <ol>
 * <li>sourceAccountId - String</li>
 * <li>beneficiaryAccountId - String</li>
 * <li>amount - Float</li>
 * </ol>
 */
public class TransferBalanceRequestDto {

	@NotBlank(message = "Source account id is mandatory")
	private String sourceAccountId;
	@NotBlank(message = "Beneficiary account id is mandatory")
	private String beneficiaryAccountId;
	@NotBlank(message = "Amount is mandatory")
	private Float amount;

	/**
	 * @return the sourceAccountId
	 */
	public String getSourceAccountId() {
		return sourceAccountId;
	}

	/**
	 * @param sourceAccountId the sourceAccountId to set
	 */
	public void setSourceAccountId(String sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}

	/**
	 * @return the beneficiaryAccountId
	 */
	public String getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}

	/**
	 * @param beneficiaryAccountId the beneficiaryAccountId to set
	 */
	public void setBeneficiaryAccountId(String beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}

	/**
	 * @return the amount
	 */
	public Float getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Float amount) {
		this.amount = amount;
	}

}
