package com.cashewpayment;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cashewpayment.dto.TransferBalanceRequestDto;
import com.cashewpayment.dto.TransferBalanceResponseDto;
import com.cashewpayment.entities.Account;
import com.cashewpayment.exception.CustomException;
import com.cashewpayment.repository.AccountRepository;
import com.cashewpayment.service.AccountBalanceTransferService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CashewPaymentAssignmentApplication.class)
public class AccountBalanceTransferTests {

	@Autowired
	private AccountBalanceTransferService accountBalanceTransferService;

	@Autowired
	private AccountRepository accountRepository;

	@Test
	public void testWhenSourceAccountIdIsNotFound() throws Exception {
		try {
			TransferBalanceRequestDto request = new TransferBalanceRequestDto();
			request.setBeneficiaryAccountId("1");
			request.setSourceAccountId("2");
			request.setAmount(10f);

			accountBalanceTransferService.transfer(request);
		} catch (RuntimeException e) {
			assertThat(e).isInstanceOf(CustomException.class).hasFieldOrPropertyWithValue("errorCode",
					"balance-transfer-001");
		}
	}

	@Test
	public void testWhenBeneficiaryAccountIdIsNotFound() throws Exception {
		try {
			TransferBalanceRequestDto request = new TransferBalanceRequestDto();
			request.setSourceAccountId("3d253e29-8785-464f-8fa0-9e4b57699db9");
			request.setBeneficiaryAccountId("1");
			request.setAmount(10f);

			accountBalanceTransferService.transfer(request);
		} catch (RuntimeException e) {
			assertThat(e).isInstanceOf(CustomException.class).hasFieldOrPropertyWithValue("errorCode",
					"balance-transfer-002");
		}
	}

	@Test
	public void testWhenBothAccountIdsAreSame() throws Exception {
		try {
			TransferBalanceRequestDto request = new TransferBalanceRequestDto();
			request.setBeneficiaryAccountId("3d253e29-8785-464f-8fa0-9e4b57699db9");
			request.setSourceAccountId("3d253e29-8785-464f-8fa0-9e4b57699db9");
			request.setAmount(10f);

			accountBalanceTransferService.transfer(request);
		} catch (RuntimeException e) {
			assertThat(e).isInstanceOf(CustomException.class).hasFieldOrPropertyWithValue("errorCode",
					"balance-transfer-003");
		}
	}

	@Test
	public void testWhenSourceAccountIdDoNotHaveSufficientBalance() throws Exception {
		try {
			TransferBalanceRequestDto request = new TransferBalanceRequestDto();
			request.setBeneficiaryAccountId("3d253e29-8785-464f-8fa0-9e4b57699db9");
			request.setSourceAccountId("17f904c1-806f-4252-9103-74e7a5d3e340");
			request.setAmount(100f);

			accountBalanceTransferService.transfer(request);
		} catch (RuntimeException e) {
			assertThat(e).isInstanceOf(CustomException.class).hasFieldOrPropertyWithValue("errorCode",
					"balance-transfer-004");
		}
	}

	@Test
	public void testWhenSourceAccountIdHaveSufficientBalance() throws Exception {
		TransferBalanceRequestDto request = new TransferBalanceRequestDto();
		request.setBeneficiaryAccountId("3d253e29-8785-464f-8fa0-9e4b57699db9");
		request.setSourceAccountId("17f904c1-806f-4252-9103-74e7a5d3e340");
		request.setAmount(10f);

		TransferBalanceResponseDto response = accountBalanceTransferService.transfer(request);
		Account sourceAccount = accountRepository.getOne(request.getSourceAccountId());
		Account beneficiaryAccount = accountRepository.getOne(request.getBeneficiaryAccountId());

		assertTrue(sourceAccount.getBalance().equals(response.getSourceAccountAmount())
				&& beneficiaryAccount.getBalance().equals(response.getBeneficiaryAccountAmount()));
	}
}
