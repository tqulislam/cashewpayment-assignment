package com.cashewpayment;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cashewpayment.entities.Account;
import com.cashewpayment.service.AccountService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CashewPaymentAssignmentApplication.class)
public class AccountServiceTests {

	@Autowired
	private AccountService accountService;

	@Test
	public void testToCheckIfDataIsLoadedInRepository() {
		List<Account> accounts = accountService.getAll();
		assertTrue(accounts.size() > 0);
	}
}
